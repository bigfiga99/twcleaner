# Discord bot to delete messages containing links to twitter.
import discord, re, requests

# Functions for parsing text and checking links. 

def is_twitter(link):
    # To get the url of links that redirect. 
    url = requests.head(link, allow_redirects=True).url
    pattern = "https://twitter.com/"

    if re.search(pattern, url):
        return True
    return False

def check_links(text):
    # Catches http/s links these links highlight and embed in discord. 
    pattern = "https?:\/\/[\w/\-?=%.]+\.[\w/\-&?=%.]+"
    lst_links = re.findall(pattern, text)

    for link in lst_links:
        if is_twitter(link):
            return True
    return False

# Code related to running the bot.

client = discord.Client()

@client.event
async def on_ready():
    # Just to see if the bot is running.
    print("Logged in as {0.user}".format(client))

# This will process every message posted after the bot has come online. 
@client.event
async def on_message(message):
    if message.author == client.user:
        return
    
    if check_links(message.content):
        await message.delete()
        await message.channel.send("Keep off twitter and keep it away from us!")

with open("token.txt", "r") as f:
    TOKEN = f.readline()

client.run(TOKEN)
